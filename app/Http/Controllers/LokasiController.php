<?php

namespace App\Http\Controllers;

use App\lokasi;
use Illuminate\Http\Request;

class LokasiController extends Controller
{
    //penamaan fungsi bebas, tidak harus index, create, store dsb.


    public function index(){
        //mengambil semua data dari tabel lokasi
        //istilah query untuk laravel adalah eloquent.

        //$data = lokasi::all(); atau
        $data = lokasi::select('*')->get();

        return view('lokasi.index',compact('data')); //compact untuk mengirim variable ke view
    }

    public function create(){
        return view('lokasi.create');
    }

    public function store(Request $request){ //gunakan parameter request untuk mengambil data dari form
        //Cara I
        //cara mass assigment, harus inisialisasi di model (protected fillable)

        //lokasi::create($data);

        //Cara II tanpa mass assigment
        lokasi::create([
            'alamat' => $request->alamat,
            'kelurahan' => $request->kelurahan,
            'kode_pos' => $request->kode_pos,
            'id_users' => $request->id_users
        ]);

        return redirect('lokasi'); //redirecting halaman ke url (routing)
    }

    public function edit($id){  //id sebagai parameter yang dikirim dari routing, nama var harus sama dengan yang dirouting
         $data = lokasi::where('id_lokasi',$id)->first();

         return view('lokasi.edit',compact('data'));
    }

    public function update($id,Request $request){

        lokasi::where('id_lokasi',$id)->update([
            'alamat' => $request->alamat,
            'kelurahan' => $request->kelurahan,
            'kode_pos' => $request->kode_pos,
            'id_users' => $request->id_users
        ]);

        return redirect('lokasi');
    }

    public function show($id_lokasi){
        $data = lokasi::where('id_lokasi',$id_lokasi)->first();

        return view('lokasi.tampilkan',compact('data'));
    }

    public function destroy($id){
        lokasi::where('id_lokasi',$id)->delete();

        return redirect('lokasi');
    }


}
