<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lokasi extends Model
{
    //
    protected $table = "lokasi_studi";

    protected $fillable = [
        'alamat',
        'kelurahan',
        'kode_pos',
        'id_users',
        'longitude',
        'latitudue'
    ];

}
