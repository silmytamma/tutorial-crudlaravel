<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); //contoh routing yang langsung redirect ke view (tanpa controller)
});

Route::get('lokasi', 'LokasiController@index');
Route::get('lokasi/tambah', 'LokasiController@create');
Route::post('lokasi/tambah', 'LokasiController@store'); //penamaan url yang sama tidak masalah jika methodnya berbeda
Route::get('lokasi/edit/{id}', 'LokasiController@edit');
Route::post('lokasi/update/{id}', 'LokasiController@update');
Route::get('lokasi/show/{id_lokasi}', 'LokasiController@show');
Route::get('lokasi/hapus/{id}','LokasiController@destroy');
