<html>
    <title>
        Edit Lokasi studi
    </title>
    <body>
        <h3>Edit Lokasi</h3>

        <form action="{{url('lokasi/update/'.$data->id_lokasi)}}" method="post">

            <!-- _token hidden wajib ada form data-->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <p> Alamat : <input type="text" name="alamat" width="200px" value="{{$data->alamat}}"> </p>
            <p> Kelurahan : <input type="text" name="kelurahan" width="200px" value="{{$data->kelurahan}}"> </p>
            <p> Kode Pos : <input type="text" name="kode_pos" width="200px" value="{{$data->kode_pos}}"> </p>
            <p> User ID : <input type="number" name="id_users" width="200px" value="{{$data->id_users}}"> </p>
            <p><button type="submit">Kirim</button> </p>
            <p><a href="{{url('lokasi')}}"> Kembali </a> </p>
        </form>
    </body>
</html>