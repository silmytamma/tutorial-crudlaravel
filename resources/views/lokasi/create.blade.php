<html>
    <title>
        Input Lokasi studi
    </title>
    <body>
        <h3>Input Lokasi</h3>

        <form action="{{url('lokasi/tambah')}}" method="post">

            <!-- _token hidden wajib ada form data-->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <p> Alamat : <input type="text" name="alamat" width="200px"> </p>
            <p> Kelurahan : <input type="text" name="kelurahan" width="200px"> </p>
            <p> Kode Pos : <input type="text" name="kode_pos" width="200px"> </p>
            <p> User ID : <input type="number" name="id_users" width="200px"> </p>
            <p><button type="submit">Kirim</button> </p>
            <p><a href="{{url('lokasi')}}"> Kembali </a> </p>
        </form>
    </body>
</html>