<html>
    <title>Test Crud</title>
    <body>
        <h3>Tabel Lokasi Studi</h3>
        <a href="{{url('lokasi/tambah')}}"> <button>Tambah </button> </a>
        <br><br>
        <table border="1">
            <thead>
                <th>No</th>
                <th>Alamat</th>
                <th>Kelurahan</th>
                <th>Kode Pos</th>
                <th>User ID</th>
                <th>Action</th>
            </thead>
            <tbody>
            @foreach($data as $i => $item)
                <tr>
                    <td>
                        {{$i++}}
                    </td>
                    <td>{{$item->alamat}}</td>
                    <td>{{$item->kelurahan}}</td>
                    <td>{{$item->kode_pos}}</td>
                    <td>{{$item->id_users}}</td>
                    <td><a href="{{url('lokasi/edit/'.$item->id_lokasi)}}"> <button>Edit</button> </a>
                        | <a href="{{url('lokasi/hapus/'.$item->id_lokasi)}}"><button>Hapus</button></a>
                        | <a href="{{url('lokasi/show/'.$item->id_lokasi)}}"><button>Show</button></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </body>
</html>